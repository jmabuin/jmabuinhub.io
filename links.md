---
layout: page
title: Links
permalink: /links/
---

* [GitHub Repository](https://github.com/jmabuin){:target="_blank"}
* [Linkedin profile](http://linkedin.com/in/josemanuelabuinmosquera){:target="_blank"}
* [CiTIUS personal page](https://citius.usc.es/equipo/investigadores-en-formacion/jose-manuel-abuin-mosquera){:target="_blank"}
* [ORCID](http://orcid.org/0000-0001-9771-818X){:target="_blank"}
* [ResearchGate profile](https://www.researchgate.net/profile/Jose_Abuin_Mosquera){:target="_blank"}
* [Google Scholar profile](https://scholar.google.es/citations?user=3-H_4c8AAAAJ){:target="_blank"}
