---
layout: page
title: Publications
permalink: /publications/
---

Publications are divided in [Journals](#journals) and [Conferences](#conferences), and sorted in chronological order.

---

## <a name="journals"></a>Journals ##

* _PASTASpark: multiple sequence alignment meets Big Data_ [(Online article)](http://dx.doi.org/10.1093/bioinformatics/btx354){:target="_blank"}  
[José M. Abuín](http://jmabuin.github.io/), [Tomás F. Pena](https://citius.usc.es/equipo/persoal-adscrito/tomas-fernandez-pena){:target="_blank"} and [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}.  
Bioinformatics, Vol. 33, Issue 18, pages 2948-2950, 2017.

* _SparkBWA: Speeding Up the Alignment of High-Throughput DNA Sequencing Data_ [(Online article)](http://dx.doi.org/10.1371/journal.pone.0155461){:target="_blank"}  
[José M. Abuín](http://jmabuin.github.io/), [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}, [Tomás F. Pena](https://citius.usc.es/equipo/persoal-adscrito/tomas-fernandez-pena){:target="_blank"} and [Jorge Amigo](https://es.linkedin.com/in/amigolechuga){:target="_blank"}.  
PLoS ONE, Vol. 11, Number 5, pages 1-21, 2016.

* _BigBWA: Approaching the Burrows-Wheeler Aligner to Big Data Technologies._ [(Online article)](http://dx.doi.org/10.1093/bioinformatics/btv506){:target="_blank"}  
[José M. Abuín](http://jmabuin.github.io/), [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}, [Tomás F. Pena](https://citius.usc.es/equipo/persoal-adscrito/tomas-fernandez-pena){:target="_blank"} and [Jorge Amigo](https://es.linkedin.com/in/amigolechuga){:target="_blank"}.  
Bioinformatics, Vol. 31, Issue 24, pages 4003-4005, 2015.

* _Análisis Morfosintáctico y Clasificación de Entidades Nombradas en un Entorno Big Data._ [(Online article)](http://journal.sepln.org/sepln/ojs/ojs/index.php/pln/article/view/5046){:target="_blank"}  
[Pablo Gamallo](https://citius.usc.es/equipo/persoal-adscrito/pablo-gamallo-otero){:target="_blank"}, [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}, [Marcos García](http://www.grupolys.org/~marcos/){:target="_blank"}, [José M. Abuín](http://jmabuin.github.io/) and [Tomás F. Pena](https://citius.usc.es/equipo/persoal-adscrito/tomas-fernandez-pena){:target="_blank"}.  
Procesamiento del Lenguaje Natural, Vol. 53, pages 17-24, 2014.

---

## <a name="conferences"></a>Conferences ##
* _Perldoop2: a Big Data-oriented source-to-source Perl-Java compiler._ [(Online article)](https://citius.usc.es/investigacion/publicacions/listado/perldoop2-big-data-oriented-source-to-source-perl-java-compiler){:target="_blank"}  
[César Piñeiro](https://citius.usc.es/equipo/investigadores-en-formacion/cesar-alfredo-pineiro-pomar){:target="_blank"}, [José M. Abuín](http://jmabuin.github.io/), [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}
IEEE International Conference on Big Data Intelligence and Computing. Orlando, USA, November 2017. 

* _Perldoop: Efficient Execution of Perl Scripts on Hadoop Clusters._ [(Online article)](http://dx.doi.org/10.1109/BigData.2014.7004303){:target="_blank"}  
[José M. Abuín](http://jmabuin.github.io/), [Juan C. Pichel](http://persoal.citius.usc.es/jcpichel/){:target="_blank"}, [Tomás F. Pena](https://citius.usc.es/equipo/persoal-adscrito/tomas-fernandez-pena){:target="_blank"}, [Pablo Gamallo](https://citius.usc.es/equipo/persoal-adscrito/pablo-gamallo-otero){:target="_blank"} and [Marcos García](http://www.grupolys.org/~marcos/){:target="_blank"}.  
IEEE Int. Conference on Big Data (IEEE Big Data). Washington D.C., USA, October 2014.

