---
layout: page
title: About
permalink: /about/
---

I was born in <i class="fa fa-map-marker" aria-hidden="true"></i>[Rianxo](https://goo.gl/maps/yiDcWZsRNyv){:target="_blank"} (Galicia) in 1982. I have a PhD in Computer Science, MSc in High Performance Computing and BSc in Computer Science, all of them at the [University of Santiago de Compostela](http://www.usc.es){:target="_blank"}.

Nowadays I work as Post-Doctoral researcher and software developer at [(CiTIUS)](http://citius.usc.es){:target="_blank"}, in the [University of Santiago de Compostela](http://www.usc.es){:target="_blank"}, while I carry out a research stay at the [Polytechnic Institute of Cávado and Ave](https://ipca.pt/){:target="_blank"}, in Barcelos (Portugal).

My previous experience is:
### [ITMATI](http://www.itmati.com/){:target="_blank"}, Technological Institute for Industrial Mathematics. ###
Post-Doctoral researcher and Software Developer

* Software development in Python, C++ and Fortran.
* Development of prediction models for time series data.

### [DXC Technology](http://www.dxc.technology/){:target="_blank"} ###
HPC Data Scientist.

* Development of Big Data software to perform ETL processes by using Apache Spark.

### Centro de Investigación en Tecnoloxías da Información [(CiTIUS)](http://citius.usc.es){:target="_blank"} ###
Researcher and PhD student. My research is based in:

* Big Data applications performance with Hadoop and Spark.
* High Performance Computing applied to Natural Language Processing and Genomics.
* Genomics alignment by using Big Data and HPC technologies.

### [Applied Mathematics Department](http://www.usc.es/dmafm/){:target="_blank"}, [University of Santiago de Compostela](http://www.usc.es){:target="_blank"} ###
Developer.

* Development of gas networks simulator and optimizator by using Fortran language.
* In charge of the development of a graphical interface for its use with the simulator and optimizator tool based in the open source project [Quantum Gis](http://www.qgis.org){:target="_blank"}. This interface is made with Pyhton and Qt (PyQt).
* Code development for mathematical problems.
* Development of paralell applications with OpenMp and MPI.
* Implementation of the software simulator and optimizator software as SaaS (Software as a Service).
* Administration of the Linux server where the gas simulator and optimizator software resides.

### Galicia Supercomputing Centre [(Cesga)](http://www.cesga.es){:target="_blank"}, e-Learning department ###
European e-Learning projects technician

* Linux servers administration (dom0 Xen servers and also virtual machines)
* Development of web platforms based on Java and Php
* Maintenance and development of an e-Learning platform based in the [Chamilo](https://chamilo.org/chamilo-lms/){:target="_blank"} Open Source platform, [Aula Cesga](https://aula.cesga.es){:target="_blank"}
* Managing videoconferences through BigBlueButton
* Scripts programming using Python and Bash

You can find more information about me and my job in the Projects section.
