---
layout: page
title: Short CV
permalink: /shortcv/
---

## Career ##

---
* **CiTIUS - University of Santiago de Compostela, and Polytechnic Institute of Cávado and Ave**  
_Post-Doctoral Researcher in Big Data and High Performance Computing for Genomics Applications_  
October 2018 - Nowadays

* **ITMATI**  
_Post-Doctoral researcher_
February 2018 - October 2018, (9 months)

* **DXC Technology**  
_HPC Data Scientist_  
September 2017 - January 2018, (5 months)  

* **CiTIUS - University of Santiago de Compostela**  
_Researcher in Big Data and High Performance Computing_  
January 2014 - August 2017, (3 years and 8 months)  

* **Applied Mathematics Department - University of Santiago de Compostela**  
_Developer and Researcher_  
February 2013 - May 2014, (1 year and 3 months)

* **Galicia Supercomputing Centre**  
_Projects technician & systems administrator_  
January 2012 - February 2013, (1 year and 1 month)  
February 2010 - September 2011, (1 year and 7 months)  
April 2008 - November 2009, (1 year and 7 months)  
Total time: 4 years and 3 months  

---

## Education ##
* **PhD (Doctor of Philosophy) Computer Science.**  
University of Santiago de Compostela.  
Dissertation: _[Big Data meets High Performance Computing: Genomics and Natural Language Processing as case studies](https://github.com/jmabuin/jmabuin.github.io/blob/master/files/PHd_Dissertation_JMAbuin.pdf){:target="_blank"}_  
Date: November 2017
Grade: Sobresaliente Cum Laude

* **MSc High Performance Computing**  
University of Santiago de Compostela.  
Date: July 2013

* **BSc Computer Science**  
University of Santiago de Compostela.  
Date: July 2012

---

## Aditional courses ##
* _Big Data developer for Apache Spark_, Centro de Novas Tecnoloxías de Galicia. (48 hours)  
* _Introduction to CUDA programming_, Galicia Supercomputing Centre. (12 hours)  
* _Python data processing:  Environments and applications_, Galicia Supercomputing Centre. (20 hours)  
* _Compiling, executing and optimizing scientific codes_, Galicia Supercomputing Centre. (10 hours)  
* _Linux Administration I_, Training Channel (20 hours)  
* _Linux Administration II_, Training Channel(20 hours)  
* _Configuring Apache_, Training Channel (2 hours)  
* _Computer Architecture_, Galicia Supercomputing Centre. (20 hours)  
* _UML Specification_, Galicia Supercomputing Centre. (20 hours)  
* _ITIL V2_, Galicia Supercomputing Centre. (26 hours)  
* _SOA/WOA Specification_, Galicia Supercomputing Centre. (24 hours)  
* _Django Framework_, Galicia Supercomputing Centre. (20 hours)  