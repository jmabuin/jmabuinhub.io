---
layout: page
title: Teaching
permalink: /teaching/
---

## Degree in Computer Engineering (USC)##

### Academic Year 2016/2017 ###

* [Digital Systems](http://www.usc.es/gl/centros/etse/materia.html?materia=106228&ano=67){:target="_blank"} [(English)](http://www.usc.es/gl/centros/etse/materia.html?materia=106228&ano=67&idioma=7){:target="_blank"}
* [Operating Systems I](http://www.usc.es/gl/centros/etse/materia.html?materia=106238&ano=67){:target="_blank"} [(English)](http://www.usc.es/gl/centros/etse/materia.html?materia=106238&ano=67&idioma=7){:target="_blank"}

### Academic Year 2015/2016 ###

* [Digital Systems](http://www.usc.es/es/centros/etse/materia.html?materia=95601&ano=66){:target="_blank"} [(English)](http://www.usc.es/es/centros/etse/materia.html?materia=95601&ano=66&idioma=7){:target="_blank"}
* [Operating Systems I](http://www.usc.es/es/centros/etse/materia.html?materia=95611&ano=66){:target="_blank"} [(English)](http://www.usc.es/en/centros/etse/materia.html?materia=95611&ano=66&idioma=7){:target="_blank"}


